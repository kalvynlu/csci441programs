#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec3 color;
in vec2 uv;
in vec3 normal;

out vec3 fcolor;
out vec2 fuv;
out vec4 P;
out vec4 N;

void main() {
  vec3 lightColor = vec3(1.0f,1.0f,1.0f);
  gl_Position = projection * view * model * vec4(position, 1);
  fcolor = color;
  fuv = uv;
  P = view * model * vec4(position, 1.0f) ;
  N = normalize(transpose(inverse(view * model)) * vec4(normal, 0.0f));
}
