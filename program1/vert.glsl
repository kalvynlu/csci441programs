#version 330

in vec2 position;
uniform mat4 matrix;
layout(location = 1) in vec3 vertexColor;
out vec3 fragmentColor;

void main() {
  gl_Position = matrix * vec4(position.x, position.y, 0, 1);
  fragmentColor = vertexColor;
}
