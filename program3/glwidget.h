#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <QTimer>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat3;
using glm::mat4;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

    public slots:
    // Part 2 - add an animate slot

    private:
        void initializeCube();
        void renderCube();



        void renderCube(float specular, float ambient, mat4 transform);

        QTimer *timer;

        float time;

        // animation sequence
        std::vector<vec3> rotations;

        // matrices for storing the end result of our animation using different techniques
        mat4 matrixLerp;
        mat4 eulerLerp;
        mat4 quatSlerp;

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;

        GLuint planeProg;
        GLuint planeVao;
        GLint planeProjMatrixLoc;
        GLint planeViewMatrixLoc;
        GLint planeModelMatrixLoc;
        GLint *texturelData;
        GLuint texture;

        void initializePlane();
        void renderPlane();

        void initializeBrick();
        void renderBrick(float ambient, float specular,float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ);
        void renderWall(float ambient, float specular,bool evenStart, int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ);
        void renderRoom(float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ);
        void renderStraightWall(float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ);
        void renderStraightRoom (float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ);

        GLuint brickProg;
        GLuint brickVao;
        GLint brickProjMatrixLoc;
        GLint brickViewMatrixLoc;
        GLint brickModelMatrixLoc;

        GLuint textureObject;
        GLuint lightPositionLoc;
        GLint ambientLoc;
        GLint specularLoc;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        float sensitivity = .5;

        // Part 1 - Add two mat4 variables for pitch and yaw.
        // Also add two float variables for the pitch and yaw angles.

        float pitch = 0;
        float yaw = 0;

        mat4 yawMatrix;
        mat4 pitchMatrix;

        // Part 2 - Add a QTimer variable for our render loop.

        // Part 3 - Add state variables for keeping track
        //          of which movement keys are being pressed
        //        - Add two vec3 variables for position and velocity.
        //        - Add a variable for toggling fly mode

        bool forward = false;
        bool back = false;
        bool left = false;
        bool right = false;
        bool flyMode = false;
        bool up = false;
        bool down = false;

        vec3 position = vec3(0,0,0);
        vec3 velocity = vec3(0,0,0);

        int width;
        int height;

        glm::vec2 lastPt;
        void updateView();
        float DegreeToRadian(float degrees);

    private slots:
        void animate();
};

#endif
