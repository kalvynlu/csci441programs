# Procedural Modeling

Procedural modeling is a term used in computer graphics to describe methods for generating geometry using a set of rules rather than using manual modeling techniques. Some advanced examples of procedural modeling are L-systems and fractals. High end procedural modeling systems are used in animated films and video games to generate natural landscapes, forests, cityscapes and more.

This program generates and renders two types of brick walls: a flat brick wall and a brick room. You can increase/decrease the size, spacing, rows, and columns of the wall.

![wall](http://snag.gy/DYkak.jpg)

* A README.md file with any necessary instructions for using your program, along with a description of what your procedurally generated object is

##Controls
* P - Increase brick size
* O - Decrease brick size
* I - Increase space between bricks
* U - Decrease space between bricks
* L - Increase rows in wall
* K - Decrease rows in wall
* J - Increase columns in wall
* H - Decrease columns in wall
* Space - Switch between brick wall and brick room
* 1/Q - increase/decrease X position
* 2/W - increase/decrease Y position
* 3/E - increase/decrease Z position


Extras:
Move light around using keyboard controls