## Summary

Virtual worlds are an important part of graphics applications. Objects 
populate a world or scene and the user is able to navigate that world in 
some way. In this assignment, you will be creating a world that the user 
can move around in using first-person controls. You must scatter around the 
objects you generated in program 2, along with some other simpler pieces of 
geometry to create an interesting world to explore. Vary the customizable 
options you had in program 2 to keep the world more interesting. Assign 
different material properties to the different objects in your world. You may 
choose the lighting conditions of your world. Lighting plays an important role 
in how your world looks and feels. Is it mid day? Dusk? Dark with a flashlight 
attached to your virtual character? You must also animate at least one object
in your world to add an extra detail to your world. For example, an object 
could be floating down a stream or a character to could be walking back and 
forth, it's up to you.

![Image of Program3](https://snag.gy/rCvzOZ.jpg)


## Controls
* WASD Movement
* Shift-Key moves downward
* Space-Key moves upward

## Program Requirements
* Walls of the room are rendered with off-center bricks (1 room, .2 Ambient, 90 specular)
* Walls within the room are rendered with straight-columned bricks (4 walls, 1 Ambient, 100 Specular)
* Walls of the base under the animated cube are rendered with straight-columned bricks with 0 (5 rooms, 0 ambient, 1 specular)
* Animated Cube has 1 Ambient with 0 specular
* Each step of the stairs is rendered by scaling a cube (10 scaled cubes)
* At the top of the stairs, is another scaled cube (1 scaled cube)




