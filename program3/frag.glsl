#version 330

uniform vec3 lightP;
uniform mat4 view;
uniform mat4 model;
uniform float specularMod;
uniform float ambientMod;


in vec3 fcolor;
in vec2 fuv;
in vec4 N;
in vec4 P;

out vec4 color_out;

void main() {
    vec3 lightColor = vec3(1.0f,1.0f,1.0f);
    vec3 normal = normalize(N.xyz);
    vec4 lp = view * vec4(lightP, 1);
    vec3 L = normalize(lp.xyz - P.xyz);

    float diffuse = clamp(dot(normal,L), 0.0f, 1.0f);
    vec3 R = 2.0f * dot(normal,L) * normal - L;
    vec3 V = normalize(-P.xyz);

    float specular = 1 * pow(max(0, dot(R,V)), specularMod);

    float ambient = ambientMod;

    color_out = vec4( fcolor * diffuse + vec3(specular) + vec3(ambient * fcolor),1);
}
