Program 1
---------

Grade Breakdown
---------------

    Position - 20/20
    Color    - 15/15
    Size     - 10/10
    Shape    - 10/10 
    Fill     - 15/15 
    Resize   - 10/10
    General  - 13/20
    -------------------------------
               93/100

Notes
-----
1. Didn't accept image from command line. -2
2. Extra triangle shape. +1
3. Extra sizes. +2
4. Averager action. This is essentially a blur operation. Look up convolution matrix, the first result (https://docs.gimp.org/en/plug-in-convmatrix.html). Figure 17.152. is essentially what you implemented.  +2 
5. Invert color mode. +2
6. Clear doesn't properly clear colors, so it leads to incorrect behavior after clearing. -2
