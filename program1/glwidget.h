#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>

// glm by default uses degrees, but that functionality
// is deprecated so GLM_FORCE_RADIANS turns off some 
// glm warnings
#define GLM_FORCE_RADIANS

using glm::vec2;

QColor getPix(int row, int col);
glm::vec3 averagePix(int x, int y);
void drawTriangle(int x, int y, glm::vec3 color);
void drawSquare(int x, int y, glm::vec3 pushColor);
void drawCircle(int x, int y, glm::vec3 pushColor);


class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();
        void mouseMoveEvent(QMouseEvent *event);
        void mousePressEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);


    private:
        GLuint loadShaders(const char* vertf, const char* fragf);
        static const GLchar* readShader(const char* filename);

        GLuint vao;
        GLuint program;

        GLuint positionBuffer;
        GLuint colorbuffer;
        bool outline;
        GLenum drawMode;

};

#endif
