#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <QTextStream>
#include <QLabel>
#include <math.h>
#include <QDir>


#define pi 3.14159265

using namespace std;
int drawCounter = 0;
int sizeMod = 1;
bool invert = false;
QImage image;
std::vector<vec2> pts;
std::vector<glm::vec3> colors;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), outline(false), drawMode(0) {
   // num_pts = 0;
    drawMode = GL_TRIANGLES;
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_C:
            cout << "Cleared all the points. Make sure to "
                    "update this once you modify how the points "
                    "are stored." << endl;
            //num_pts = 0;
            pts.clear();
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
            break;
        case Qt::Key_W:
            outline = !outline;
            if(outline) {
                cout << "Displaying outlines." << endl;
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            } else {
                cout << "Displaying filled polygons." << endl;
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
            break;
        case Qt::Key_Space:
            drawCounter = (drawCounter + 1) % 3;
            break;
        case Qt::Key_P:
            sizeMod++;
            break;
        case Qt::Key_O:
            sizeMod--;
            break;
        case Qt::Key_U: 
        {
            std::cout << "Filling..." << endl;
            for(int i = 0; i < image.width(); i+= sizeMod){
                for(int j = 0; j < image.height(); j+= sizeMod){
                    glm::vec3 pixColor;
                    pixColor.r = getPix(i,j).redF();
                    pixColor.g = getPix(i,j).greenF();
                    pixColor.b = getPix(i,j).blueF();
                    switch(drawCounter){
                        case 0:
                            drawTriangle(i,j,pixColor);
                        break;
                        case 1:
                            drawSquare(i,j,pixColor);
                        break;
                        case 2:
                            drawCircle(i,j,pixColor);
                        break;
                    }
                }
            }
            glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
            glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);
            update();
            std::cout<<"Done!" << endl;
        }
                break;
        case Qt::Key_A: //averager
        {
            std::cout<< "Averaging..." << endl;
            for(int i = 1; i < image.width()-1; i+= sizeMod){
                for(int j = 1; j < image.height()-1; j+= sizeMod){
                    glm::vec3 pixColor = averagePix(i,j);
                    switch(drawCounter){
                        case 0:
                            drawTriangle(i,j,pixColor);
                        break;
                        case 1:
                            drawSquare(i,j,pixColor);
                        break;
                        case 2:
                            drawCircle(i,j,pixColor);
                        break;
                    }
                }
            }
            glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
            glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);
            update();
            std::cout << "Done!" << endl;
        }

            break;
    case Qt::Key_I:
        invert = !invert;
        break;
    }

    update();
}

glm::vec3 averagePix(int x, int y){
    float r = 0;
    float g = 0;
    float b = 0;
    glm::vec3 avrg;
    for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++){
                r = r + getPix(x + i,y + j).redF();
                g = g + getPix(x + i,y + j).greenF();
                b = b + getPix(x + i, y + j).blueF();
        }
    }
    r = r/9;
    g = g/9;
    b = b/9;
    avrg.r = r;
    avrg.g = g;
    avrg.b = b;
    return avrg;
}


QColor getPix(int x, int y){
     QColor clrCurrent( image.pixel( x, y ) );
     if(invert){
        clrCurrent.setRedF(1 - clrCurrent.redF());
        clrCurrent.setGreenF(1 - clrCurrent.greenF());
        clrCurrent.setBlueF(1 - clrCurrent.blueF());
     }
     return clrCurrent;
}

void drawTriangle(int x, int y, glm::vec3 pushColor) {
    vec2 p1,p2,p3;
    //Top point
    p1.x = x;
    p1.y = y - (sizeMod * 2);
    //Left point
    p2.x = x - sizeMod;
    p2.y = y;
    //Right point
    p3.x = x + sizeMod;
    p3.y = y;
    //Array Add
    pts.push_back(p1);
    pts.push_back(p2);
    pts.push_back(p3);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
}

void drawSquare(int x, int y, glm::vec3 pushColor){
    vec2 p1,p2,p3,p4;
    //First triangle
    //Top Left
    p1.x = x;
    p1.y = y;
    //Bottom Left
    p2.x = x;
    p2.y = y + sizeMod * 2;
    //Top Right
    p3.x = x + sizeMod * 2;
    p3.y = y;
    //Bottom Right
    p4.x = x + sizeMod * 2;
    p4.y = y + sizeMod * 2;
    //Array Add
    pts.push_back(p1);
    pts.push_back(p2);
    pts.push_back(p3);
    pts.push_back(p4);
    pts.push_back(p2);
    pts.push_back(p3);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
    colors.push_back(pushColor);
}

void drawCircle(int x, int y, glm::vec3 pushColor){
    int circleSides = 25;
    vec2 centerPoint;
    centerPoint.x = x;
    centerPoint.y = y;
    for(int i = 0; i < circleSides; i++){
        //FirstPoint
        vec2 p1;
        p1.x = x + (sizeMod * cos(i * 2 * pi / circleSides));
        p1.y = y + (sizeMod * sin(i * 2 * pi / circleSides));
        pts.push_back(p1);
        //SecondPoint
        vec2 p2;
        p2.x = x + (sizeMod * cos((i + 1) *  2 * pi / circleSides));
        p2.y = y + (sizeMod * sin((i + 1) *  2 * pi / circleSides));
        pts.push_back(p2);
        //CenterPoint
        pts.push_back(centerPoint);
        colors.push_back(pushColor);
        colors.push_back(pushColor);
        colors.push_back(pushColor);;
    }
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    glm::vec3 pushColor;
    pushColor.r = getPix(event->x(),event->y()).redF();
    pushColor.g = getPix(event->x(),event->y()).greenF();
    pushColor.b = getPix(event->x(),event->y()).blueF();
    switch(drawCounter){
        case 0: //Draw Triangles
            drawTriangle(event->x(),event->y(),pushColor);
            break;
        case 1: //Draw squares
            drawSquare(event->x(),event->y(),pushColor);
            break;
        case 2: //Draw Circles
            drawCircle(event->x(),event->y(),pushColor);
            break;
    }
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);
    update();
}
void GLWidget::mousePressEvent(QMouseEvent *event) {
    glm::vec3 pushColor;
    pushColor.r = getPix(event->x(),event->y()).redF();
    pushColor.g = getPix(event->x(),event->y()).greenF();
    pushColor.b = getPix(event->x(),event->y()).blueF();
    switch(drawCounter){
        case 0: //Draw Triangles
            drawTriangle(event->x(),event->y(),pushColor);
            break;
        case 1: //Draw squares
            drawSquare(event->x(),event->y(),pushColor);
            break;
        case 2: //Draw Circles
            drawCircle(event->x(),event->y(),pushColor);
            break;
    }
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);
    update();
}


void GLWidget::initializeGL() {
    std::cout << "Enter absolute path of image" << endl;
    QTextStream s(stdin);
    QString value = s.readLine();
    image.load(value);
    resize(image.width()/this->devicePixelRatio(),image.height()/this->devicePixelRatio());
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &positionBuffer);

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

    //Colors
    glGenBuffers(1, &colorbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,0);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    program = loadShaders(":/vert.glsl", ":/frag.glsl");

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glm::mat4 projection = glm::ortho(0.0f, 640.0f, 480.0f ,0.0f);
    GLint matrixId = glGetUniformLocation(program, "matrix");
    glUniformMatrix4fv(matrixId ,1, GL_FALSE, &projection[0][0]);
}

void GLWidget::resizeGL(int w, int h) {
    glViewport(0,0,w,h);

    // When our OpenGL context is resized, we need to change
    // our projection matrix to match. Create an orthographic
    // projection matrix that converts window coordinates to normalized
    // device coordinates.  This is similar to our previous lab,
    // except the conversion will happen in our vertex shader.
    // This way we won't need to do any conversions on our mouse
    // event coordinates and when we resize the window the geometry
    // won't be scaled unevenly.

    glUseProgram(program);
    glm::mat4 projection = glm::ortho(0.0f, (float)w, (float)h,0.0f);
    GLint matrixId = glGetUniformLocation(program, "matrix");
    glUniformMatrix4fv(matrixId ,1, GL_FALSE, &projection[0][0]);
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    glBindVertexArray(vao);
    // draw primitives based on the current draw mode
    glDrawArrays(drawMode, 0, pts.size());
}

// Copied from LoadShaders.cpp in the the oglpg-8th-edition.zip
// file provided by the OpenGL Programming Guide, 8th edition.
const GLchar* GLWidget::readShader(const char* filename) {
#ifdef WIN32
        FILE* infile;
        fopen_s( &infile, filename, "rb" );
#else
    FILE* infile = fopen( filename, "rb" );
#endif // WIN32

    if ( !infile ) {
#ifdef _DEBUG
        std::cerr << "Unable to open file '" << filename << "'" << std::endl;
#endif /* DEBUG */
        return NULL;
    }

    fseek( infile, 0, SEEK_END );
    int len = ftell( infile );
    fseek( infile, 0, SEEK_SET );

    GLchar* source = new GLchar[len+1];

    fread( source, 1, len, infile );
    fclose( infile );

    source[len] = 0;

    return const_cast<const GLchar*>(source);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}

