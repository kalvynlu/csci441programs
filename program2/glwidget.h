#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat4;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);

    private:
        void initializeBrick();
        void renderBrick(float x, float y, float z, float rotation);\

        GLuint brickProg;
        GLuint brickVao;
        GLint brickProjMatrixLoc;
        GLint brickViewMatrixLoc;
        GLint brickModelMatrixLoc;
        GLint brickInverseVMMatric;
        GLint brickTranslateMatrixLoc;
        GLint brickScaleMatrixLoc;
        GLint brickRotateMatrixLoc;


        void initializeGrid();
        void renderGrid();

        void renderRow(int count, float x, float y, float z, float rotation);
        void renderWall(int rows, int columns, float x, float y, float z, float rotation);
        void renderRoom(int rows, int columns, float x, float y, float z);
        void keyPressEvent(QKeyEvent *event);

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;
        GLuint LightPosition;
        GLint gridRotateMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;
        mat4 brickScale;
        mat4 rotateBall;
        GLint rotateBrickMatrixLoc;

        int width;
        int height;

        glm::vec3 lastVPt;
        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);

        float brickSize;
        float brickSpace;
        int wallRows;
        int wallColumns;
        int wallType;

        float lightX;
        float lightY;
        float lightZ;
    };

#endif
