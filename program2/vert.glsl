#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 lightP;
uniform mat4 translation;
uniform mat4 scale;
uniform mat4 rotateBall;
uniform mat4 rotateBrick;

in vec3 position;
in vec3 color;
in vec3 normal;
out vec3 fcolor;
out vec4 P;
out vec4 N;

void main() {
  vec4 newPos = translation *  vec4(position, 1);
  gl_Position = projection * view * scale * rotateBall * rotateBrick * model * newPos;
  fcolor = color;
  P = view  * model * newPos;
  N = normalize(transpose(inverse(view * model)) * vec4(normal, 0.0f));
}
