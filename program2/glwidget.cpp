#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);


    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
    gridRotateMatrixLoc = glGetUniformLocation(gridProg, "rotateBall");

}

void GLWidget::initializeBrick() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &brickVao);
    glBindVertexArray(brickVao);

    brickSize = 1;

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 normal[]{
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        //bottom
        vec3(0,-1,0),
        vec3(0,-1,0),
        vec3(0,-1,0),
        vec3(0,-1,0),

        //front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        //back
        vec3(0,0,-1),
        vec3(0,0,-1),
        vec3(0,0,-1),
        vec3(0,0,-1),

        //right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),

        //left
        vec3(-1,0,0),
        vec3(-1,0,0),
        vec3(-1,0,0),
        vec3(-1,0,0),
    };



    vec3 pts[] = {
        // top
        vec3(1,.5,.5),    // 0
        vec3(1,.5,-.5),   // 1
        vec3(-1,.5,-.5),  // 2
        vec3(-1,.5,.5),   // 3

        // bottom
        vec3(1,-.5,.5),   // 4
        vec3(-1,-.5,.5),  // 5
        vec3(-1,-.5,-.5), // 6
        vec3(1,-.5,-.5),  // 7

        // front
        vec3(1,.5,.5),    // 8
        vec3(-1,.5,.5),   // 9
        vec3(-1,-.5,.5),  // 10
        vec3(1,-.5,.5),   // 11

        // back
        vec3(-1,-.5,-.5), // 12
        vec3(-1,.5,-.5),  // 13
        vec3(1,.5,-.5),   // 14
        vec3(1,-.5,-.5),  // 15

        // right
        vec3(1,-.5,.5),   // 16
        vec3(1,-.5,-.5),  // 17
        vec3(1,.5,-.5),   // 18
        vec3(1,.5,.5),     // 19

        // left
        vec3(-1,-.5,.5),  // 20
        vec3(-1,.5,.5),   // 21
        vec3(-1,.5,-.5),  // 22
        vec3(-1,-.5,-.5) // 23
    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        // bottom
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        // front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        // back
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),


        // left
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0)
    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normal), normal, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    brickProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    brickProjMatrixLoc = glGetUniformLocation(program, "projection");
    brickViewMatrixLoc = glGetUniformLocation(program, "view");
    brickModelMatrixLoc = glGetUniformLocation(program, "model");
    brickInverseVMMatric = glGetUniformLocation(program, "inverseVM");
    LightPosition = glGetUniformLocation(program, "lightP");
    glUniform3f(LightPosition, lightX, lightY, lightZ);

    brickRotateMatrixLoc = glGetUniformLocation(brickProg, "rotateBall");

}


void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    brickSpace = 1.1;
    wallRows = 1;
    wallColumns = 1;
    wallType = 0;

    lightX = 0;
    lightY = 3;
    lightZ = 0;

    rotateBall = mat4(1.f);

    initializeGrid();
    initializeBrick();
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    viewMatrix = lookAt(vec3(0,0,-10),vec3(0,0,0),vec3(0,1,0));
    modelMatrix = mat4(1.0f);

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(brickProg);
    glUniformMatrix4fv(brickProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(brickViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(brickModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(brickProg);

    renderGrid();
    switch(wallType){
        case 0:
            renderWall(wallRows,wallColumns,0,0,0,0);
        break;
        case 1:
            renderRoom(wallRows,wallColumns,0.f,0.f,0.f);
        break;
    }


}

void GLWidget::renderBrick(float x, float y, float z, float rotation) {
    glUseProgram(brickProg);
    glBindVertexArray(brickVao);


    mat4 transMatrix = glm::translate(glm::mat4(1.f), glm::vec3(x, y, z));
    brickTranslateMatrixLoc = glGetUniformLocation(brickProg, "translation");
    glUniformMatrix4fv(brickTranslateMatrixLoc, 1, false, value_ptr(transMatrix));


    brickScale = glm::scale(modelMatrix, glm::vec3(brickSize,brickSize,brickSize));
    brickScaleMatrixLoc = glGetUniformLocation(brickProg, "scale");
    glUniformMatrix4fv(brickScaleMatrixLoc, 1, false, value_ptr(brickScale));

    glUniformMatrix4fv(brickRotateMatrixLoc, 1, false, value_ptr(rotateBall));


    mat4 rotateBrickMatrix = rotate(mat4(1.0f), rotation, vec3(0,1,0));
    rotateBrickMatrixLoc = glGetUniformLocation(brickProg, "rotateBrick");
    glUniformMatrix4fv(rotateBrickMatrixLoc, 1, false, value_ptr(rotateBrickMatrix));

    glUniformMatrix4fv(brickRotateMatrixLoc, 1, false, value_ptr(rotateBall));

    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);

}

void GLWidget::renderRow(int count, float x, float y, float z,float rotation) {
    float brickX = x - (count/2) * (brickSpace);
    float brickY = y;
    float brickZ = z;
    for(int i = 0; i < count; i++){
        renderBrick(brickX, brickY, brickZ, rotation);
        brickX = brickX + brickSpace;
    }
}

void GLWidget::renderWall(int rows, int columns, float x, float y, float z,float rotation) {
    for(int i = 0; i < rows; i++) {
        renderRow(columns,x,y + (i * brickSpace/2),z, rotation);
    }
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_LINES, 0, 84);
}

void GLWidget::renderRoom(int rows, int columns, float x, float y, float z) {

    renderWall(rows,columns,0,0,brickSize * columns/2,0);
    renderWall(rows,columns,0,0,-brickSize * columns/2,0);
    renderWall(rows,columns,0,0,brickSize * columns/2,1.57);
    renderWall(rows,columns,0,0,-brickSize * columns/2,1.57);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastVPt = normalize(pointOnVirtualTrackball(pt));
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec3 vPt = normalize(pointOnVirtualTrackball(pt));

    vec3 axis = cross(lastVPt, vPt);
    if(length(axis) >= .00001) {
        axis = normalize(axis);
        float angle = acos(dot(vPt,lastVPt));
        rotateBall = rotateBall * rotate(mat4(1.0f), angle, axis);

        glUseProgram(gridProg);
        glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));

        glUniformMatrix4fv(gridRotateMatrixLoc, 1, false, value_ptr(rotateBall));

        glUseProgram(brickProg);
        glUniformMatrix4fv(brickModelMatrixLoc, 1, false, value_ptr(modelMatrix));

        glUniformMatrix4fv(brickRotateMatrixLoc, 1, false, value_ptr(rotateBall));
    }
    lastVPt = vPt;
    update();
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    float r = .5f;
    float rr = r*r;
    vec3 p;
    p.x = -1 + pt.x*(2.0f/width);
    p.y = -(float)height/width*(1-pt.y*(2.0f/height));

    float xx = p.x*p.x;
    float yy = p.y*p.y;

    if(xx+yy <= rr*.5) {
        p.z = sqrt(rr-(xx+yy));
    } else {
        p.z = rr*.5/sqrt(xx+yy);
    }
    return p;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    glUseProgram(brickProg);
    switch(event->key()) {
        case Qt::Key_P:
            brickSize += .1;
            update();
        break;
        case Qt::Key_O:
            brickSize -= .1;
            if(brickSize < .1) brickSize = .1;
            update();
        break;
        case Qt::Key_I:
            brickSpace += .1;
            update();
        break;
        case Qt::Key_U:
            brickSpace -= .1;
            if (brickSpace < .1) brickSpace = .1;
            update();
        break;
        case Qt::Key_L:
            wallRows++;
            update();
        break;
        case Qt::Key_K:
            wallRows--;
            if(wallRows < 1) wallRows = 1;
            update();
        break;
        case Qt::Key_J:
            wallColumns++;
            update();
        break;
        case Qt::Key_H:
            wallColumns--;
            if(wallColumns < 1) {
                wallColumns = 1;
            }
            update();
        break;
        case Qt::Key_Space:
            wallType = (wallType + 1) % 2;
            update();
        break;
        case Qt::Key_1:
            lightX++;
            std::cout << "Lx" << lightX << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;
        case Qt::Key_Q:
            lightX--;
            std::cout << "Lx" << lightX << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;
        case Qt::Key_2:
            lightY++;
            std::cout << "Ly" << lightY << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;
        case Qt::Key_W:
            lightY--;
            std::cout << "Ly" << lightY << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;
        case Qt::Key_3:
            lightZ++;
            std::cout << "Lz" << lightZ << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;
        case Qt::Key_E:
            lightZ--;
            std::cout << "Lz" << lightZ << endl;
            glUniform3f(LightPosition, lightX, lightY, lightZ);
            update();
        break;



    }
}
