

## Summary

Non-photorealistic rendering is an area of computer graphics that involves
creating stylized digital art. It ranges from cartoon rendering to making
painting-like imagery. In this assignment you will be writing a program that
takes an image as input and allows the user to turn the image into a painterly
version of it. The user will do so by clicking and dragging on the window which
will draw OpenGL primitives which inherit the color of the corresponding pixel
in the image. Here are some examples of photos and what your program may be
able to generate from them.

| Original                          | NPR rendering                          |
|-----------------------------------|----------------------------------------|
| ![original](http://www.keenthemes.com/preview/metronic/theme/assets/global/plugins/jcrop/demos/demo_files/image1.jpg) | ![NPR rendering](http://snag.gy/dWkZJ.jpg) |

## Required Materials

Your program1 directory must include:

* All source code for the completed program
* A photo to be used as input to the program
* A screenshot of your program displaying a non-photorealistic version of your photo
* A README.md file with any necessary instructions for using your program, along with a write up on why you chose the provided image

##After starting the program
Enter the absolute path of your image file and press enter

##Controls
* P : Size of brush stroke is larger
* O : Size of brush stroke is smaller
* U : Fills the screen with the size of current brush stroke and size
* A : Averager
* I : InvertToggle
* C : Clears the screen

* SpaceBar : Changes shape of brush stroke

##Extras

* Averager: Fills the screen like the "Fill" control, but each strokes' color is the average color of its surrounding pixels.

* InvertToggle : Toggle inverted colors on strokes and fills.

##Writeup

I chose this picture because it was one of the first results on Google Images when I searched "image". It's not the most interesting picture in the world. It's just a leaf. The photography is very good, though. The painterly version is only barely recognizable after a size of 10 brush stroke (pressing P 9 times), but that might be because the picture is not very large and is very simple.
