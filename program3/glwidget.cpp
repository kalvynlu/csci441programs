#include "glwidget.h"
#include <iostream>
#include <QOpenGLTexture>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);

    matrixLerp = mat4(1.0f);
    eulerLerp = mat4(1.0f);
    quatSlerp = mat4(1.0f);

    time = 0;

    // rotations is an array of key frame euler angle orientations
    // feel free to change or add more key frames to the rotations
    // array.
    rotations.push_back(vec3(0, 0, 0));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));
}

GLWidget::~GLWidget() {
}
void GLWidget::initializeGrid() {
    // Create a new Vertex Array Object on the GPU which
     // saves the attribute layout of our vertices.
     glGenVertexArrays(1, &gridVao);
     glBindVertexArray(gridVao);

     // Create a buffer on the GPU for position data
     GLuint positionBuffer;
     glGenBuffers(1, &positionBuffer);

     GLuint colorBuffer;
     glGenBuffers(1, &colorBuffer);

     GLuint indexBuffer;
     glGenBuffers(1, &indexBuffer);

     GLuint uvBuffer;
     glGenBuffers(1, &uvBuffer);

     // Part 1 - Create a texture coordinates buffer and populate it with data. This is just like any other vertex attribute
     // that you've created so far such as color, or normals except a texture coordinate is a 2 element vector (glm::vec2).



     vec3 pts[] = {
         // top
         vec3(25,-.5,25),    // 0
         vec3(25,-.5,-25),   // 1
         vec3(-25,-.5,-25),  // 2
         vec3(-25,-.5,25),   // 3
     };

     for(int i = 0; i < 24; i++) {
         pts[i] *= .5;
     }

     vec2 uvPoints[24];

     for(int i = 0; i < 24; i+= 4){
         uvPoints[i] = vec2(1,1);
         uvPoints[i + 1] = vec2(0,1);
         uvPoints[i + 2] = vec2(0,0);
         uvPoints[i + 3] = vec2(1,0);
     }


     GLuint restart = 0xFFFFFFFF;
     GLuint indices[] = {
         0,1,2,3, restart,
         4,5,6,7, restart,
         8,9,10,11, restart,
         12,13,14,15, restart,
         16,17,18,19, restart,
         20,21,22,23
     };

     glEnable(GL_DEPTH_TEST);
     glPrimitiveRestartIndex(restart);
     glEnable(GL_PRIMITIVE_RESTART);

     // Part 2 a-d

     width = 8;
     height = 8;

     texturelData = new GLint[width * height];


     for(int i = 0; i < height ; i++){
         for(int j = 0; j < width; j ++){
             if(i % 2 == 1) {
                 if(j % 2 == 1){
                     texturelData[i*width + j] = 0xFFFFFFFF;
                 }
                 else{
                     texturelData[i*width + j] = 0x00000000;
                 }
             }else {
                 if(j % 2 == 0){
                     texturelData[i*width + j] = 0xFFFFFFFF;
                 }
                 else{
                     texturelData[i*width + j] = 0x00000000;
                 }
             }
         }
     }

     glGenTextures(1, &texture);
     glBindTexture(GL_TEXTURE_2D, texture);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, texturelData);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

     // Upload the position data to the GPU, storing
     // it in the buffer we just allocated.
     glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
     glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
     glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

     glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
     glBufferData(GL_ARRAY_BUFFER, sizeof(uvPoints), uvPoints, GL_STATIC_DRAW);

     // Load our vertex and fragment shaders into a program object
     // on the GPU
     GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
     glUseProgram(program);
     gridProg = program;

     // Bind the attribute "position" (defined in our vertex shader)
     // to the currently bound buffer object, which contains our
     // position data for a single triangle. This information
     // is stored in our vertex array object.
     glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
     GLint positionIndex = glGetAttribLocation(program, "position");
     glEnableVertexAttribArray(positionIndex);
     glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

     glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
     GLint colorIndex = glGetAttribLocation(program, "color");
     glEnableVertexAttribArray(colorIndex);
     glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

     glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
     GLint uvIndex = glGetAttribLocation(program, "uv");
     glEnableVertexAttribArray(uvIndex);
     glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

     gridProjMatrixLoc = glGetUniformLocation(program, "projection");
     gridViewMatrixLoc = glGetUniformLocation(program, "view");
     gridModelMatrixLoc = glGetUniformLocation(program, "model");
}

void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint uvBuffer;
    glGenBuffers(1, &uvBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    float ambient = .2;
    float specular = 90;

    vec3 normal[]{
           // top
           vec3(0,1,0),
           vec3(0,1,0),
           vec3(0,1,0),
           vec3(0,1,0),

           //bottom
           vec3(0,-1,0),
           vec3(0,-1,0),
           vec3(0,-1,0),
           vec3(0,-1,0),

           //front
           vec3(0,0,1),
           vec3(0,0,1),
           vec3(0,0,1),
           vec3(0,0,1),

           //back
           vec3(0,0,-1),
           vec3(0,0,-1),
           vec3(0,0,-1),
           vec3(0,0,-1),

           //right
           vec3(1,0,0),
           vec3(1,0,0),
           vec3(1,0,0),
           vec3(1,0,0),

           //left
           vec3(-1,0,0),
           vec3(-1,0,0),
           vec3(-1,0,0),
           vec3(-1,0,0),
       };

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1) // 23

    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    
        vec3(0,1,0),    

        // bottom
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  
        vec3(0,.5f,0),  

        // front
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    
        vec3(0,0,1),    

        // back
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),  
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    
        vec3(1,0,0),    


        // left
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0),  
        vec3(.5f,0,0)  
    };

    vec2 uvs[] = {
        // top
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // front
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // back
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // right
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // left
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0)

    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normal), normal, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    GLint uvIndex = glGetAttribLocation(program, "uv");
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");
    lightPositionLoc = glGetUniformLocation(program, "lightP");
    specularLoc = glGetUniformLocation(program, "specularMod");
    ambientLoc = glGetUniformLocation(program, "ambientMod");
    glUniform3f(lightPositionLoc, 0,1, 0);
    glUniform1f(specularLoc, specular);
    glUniform1f(ambientLoc, ambient);
}

void GLWidget::initializeBrick() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &brickVao);
    glBindVertexArray(brickVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint uvBuffer;
    glGenBuffers(1, &uvBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    float ambient = .1;
    float specular = 90;

    vec3 normal[]{
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        //bottom
        vec3(0,-1,0),
        vec3(0,-1,0),
        vec3(0,-1,0),
        vec3(0,-1,0),

        //front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        //back
        vec3(0,0,-1),
        vec3(0,0,-1),
        vec3(0,0,-1),
        vec3(0,0,-1),

        //right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),

        //left
        vec3(-1,0,0),
        vec3(-1,0,0),
        vec3(-1,0,0),
        vec3(-1,0,0),
    };



    vec3 pts[] = {
        // top
        vec3(1,.5,.5),    // 0
        vec3(1,.5,-.5),   // 1
        vec3(-1,.5,-.5),  // 2
        vec3(-1,.5,.5),   // 3

        // bottom
        vec3(1,-.5,.5),   // 4
        vec3(-1,-.5,.5),  // 5
        vec3(-1,-.5,-.5), // 6
        vec3(1,-.5,-.5),  // 7

        // front
        vec3(1,.5,.5),    // 8
        vec3(-1,.5,.5),   // 9
        vec3(-1,-.5,.5),  // 10
        vec3(1,-.5,.5),   // 11

        // back
        vec3(-1,-.5,-.5), // 12
        vec3(-1,.5,-.5),  // 13
        vec3(1,.5,-.5),   // 14
        vec3(1,-.5,-.5),  // 15

        // right
        vec3(1,-.5,.5),   // 16
        vec3(1,-.5,-.5),  // 17
        vec3(1,.5,-.5),   // 18
        vec3(1,.5,.5),     // 19

        // left
        vec3(-1,-.5,.5),  // 20
        vec3(-1,.5,.5),   // 21
        vec3(-1,.5,-.5),  // 22
        vec3(-1,-.5,-.5) // 23
    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        // bottom
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),

        // front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        // back
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),


        // left
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0)
    };

    vec2 uvs[] = {
        // top
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // front
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // back
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // right
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // left
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0)

    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normal), normal, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    brickProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    GLint uvIndex = glGetAttribLocation(program, "uv");
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    brickProjMatrixLoc = glGetUniformLocation(program, "projection");
    brickViewMatrixLoc = glGetUniformLocation(program, "view");
    brickModelMatrixLoc = glGetUniformLocation(program, "model");
    lightPositionLoc = glGetUniformLocation(program, "lightP");
    specularLoc = glGetUniformLocation(program, "specularMod");
    ambientLoc = glGetUniformLocation(program, "ambientMod");
    glUniform3f(lightPositionLoc, 0, 1, 0);
    glUniform1f(specularLoc, specular);
    glUniform1f(ambientLoc, ambient);
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this, SLOT(animate()));
    timer->start(16);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    initializeCube();
    initializeBrick();
    initializeGrid();

    viewMatrix = mat4(1.0f);
    modelMatrix = mat4(1.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(brickProg);
    glUniformMatrix4fv(brickViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(brickModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, .01f, 100.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(brickProg);
    glUniformMatrix4fv(brickProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderGrid();
    renderCube(90,0,glm::translate(mat4(1.0f), vec3(0,3,6))*eulerLerp);
    renderRoom(.2,90,10,19,1,1,1,0,vec3(0,1,0),0,0,0);
    //renderStraightRoom(3, 3, 1, 1, 1, 0, vec3(0,1,0), 0, 0, 0);
    renderStraightWall(1.f,100.f,3,3,1,1,1,1.5708,vec3(0,1,0),0,0,(3 + (.1 * (3 - 1)))/2);
    renderStraightWall(1.f,100.f,3,3,1,1,1,4.71239,vec3(0,1,0),0,0,(3 + (.1 * (3 - 1)))/2);
    renderStraightWall(1.f,100.f,3,7,1,1,1,0,vec3(0,1,0),5.5,0,0);
    renderStraightWall(1.f,100.f,3,7,1,1,1,0,vec3(0,1,0),-5.5,0,0);

    for(int i = 0; i < 6; i ++){
        renderStraightRoom(0.f, 1.f, i, i, 1,1,1, 0, vec3(0,1,0),0,0,6);
    }

    for(int i = 0; i < 10; i++) {
        renderCube(90.f,1.f,glm::scale(mat4(1.0f), vec3(4,-1 * (i + 1)*.5,.5)) *glm::translate(mat4(1.0f), vec3(0,0,-1 * (i + 1) - 5)));
    }

    renderCube(90.f,1.f,glm::scale(mat4(1.0f), vec3(10,.5,3)) * glm::translate(mat4(1.0f), vec3(0,4.5,-3.1)));
}

void GLWidget::renderCube(float specular, float ambient,mat4 transform) {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
    glUniform1f(specularLoc, specular);
    glUniform1f(ambientLoc, ambient);
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(transform));
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderCube() {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);

    glBindTexture(GL_TEXTURE_2D, textureObject);

    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderBrick(float ambient, float specular,float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ) {
    glUseProgram(brickProg);
    glBindVertexArray(brickVao);

    glUniform1f(specularLoc, specular);
    glUniform1f(ambientLoc, ambient);

    glUniformMatrix4fv(brickModelMatrixLoc, 1, false, value_ptr(modelMatrix * glm::scale(mat4(1.0f), vec3(scaleX, scaleY, scaleZ))
                                                                * glm::rotate(modelMatrix, rotateAngle, axis) *
                                                                glm::translate(glm::mat4(1.f), glm::vec3(translateX, translateY, translateZ))));


    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderWall(float ambient, float specular,bool smallStart, int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ) {
    float brickSpace = .1;
    float oddStart;
    float evenStart;

    if (smallStart) {
        oddStart = translateX + (brickSpace * (columns + scaleX)/2) - ((scaleX) * columns)/2;
        evenStart = translateX + (brickSpace * (columns + scaleX)/2) - ((scaleX) * columns)/2 + scaleX/2;
        for(int i = 0; i < rows; i++){
            float start = (i % 2 == 0) ? evenStart : oddStart;
            if(i % 2 == 0) {
                for(float j = 0; j < columns; j++) {
                    renderBrick(ambient, specular,scaleX, scaleY, scaleZ, rotateAngle, axis, start + ((scaleX + brickSpace) * j), (i * (scaleY/2 + brickSpace/2)) + translateY, translateZ);
                }
            } else {
                for(float j = 0; j < columns + 1; j++) {
                    renderBrick(ambient, specular,scaleX, scaleY, scaleZ, rotateAngle, axis, start + ((scaleX + brickSpace) * j), (i * (scaleY/2 + brickSpace/2)) + translateY, translateZ);
                }
            }
        }
    } else {
        evenStart = translateX + (brickSpace * (columns + scaleX)/2) - ((scaleX) * columns)/2;
        oddStart = translateX + (brickSpace * (columns + scaleX)/2) - ((scaleX) * columns)/2 + scaleX/2;
        for(int i = 0; i < rows; i++){
            float start = (i % 2 == 0) ? evenStart : oddStart;
            if(i % 2 == 0) {
                for(float j = 0; j < columns + 1; j++) {
                    renderBrick(ambient, specular,scaleX, scaleY, scaleZ, rotateAngle, axis, start + ((scaleX + brickSpace) * j), (i * (scaleY/2 + brickSpace/2)) + translateY, translateZ);
                }
            } else {
                for(float j = 0; j < columns; j++) {
                    renderBrick(ambient, specular,scaleX, scaleY, scaleZ, rotateAngle, axis, start + ((scaleX + brickSpace) * j), (i * (scaleY/2 + brickSpace/2)) + translateY, translateZ);
                }
            }
        }
    }
}

void GLWidget::renderRoom(float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ) {
    float brickSpace = .1;
    renderWall(ambient, specular,true,rows,columns,scaleX,scaleY,scaleZ,rotateAngle,axis,translateX - (brickSpace * columns),translateY,translateZ + columns * (scaleX + brickSpace)/2);
    renderWall(ambient, specular,true,rows,columns,scaleX,scaleY,scaleZ,rotateAngle + 3.14159,axis,translateX- (brickSpace * columns),translateY,translateZ + columns * (scaleX + brickSpace)/2);
    renderWall(ambient, specular,false,rows,columns,scaleX,scaleY,scaleZ,rotateAngle + 1.5708,axis,translateX- (brickSpace * columns),translateY,translateZ + columns * (scaleX)/2);
    renderWall(ambient, specular,false,rows,columns,scaleX,scaleY,scaleZ,rotateAngle + 4.71239,axis,translateX- (brickSpace * columns),translateY,translateZ + columns * (scaleX)/2);

}

void GLWidget:: renderStraightWall(float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ) {
    float brickSpace = .1;
    float startingPoint = translateX - (((columns - 1) * brickSpace) + ((columns-1) * scaleX))/2;
    for(int i = 0; i < rows; i++) {
         for(int j = 0; j < columns; j++) {
            renderBrick(ambient, specular,scaleX, scaleY, scaleZ, rotateAngle, axis, startingPoint + (j * scaleX) + (j * brickSpace), translateY + (i * .5) + (i * brickSpace), translateZ);
        }
    }
}

void GLWidget:: renderStraightRoom (float ambient, float specular,int rows, int columns, float scaleX, float scaleY, float scaleZ, float rotateAngle, vec3 axis, float translateX, float translateY, float translateZ) {
    renderStraightWall(ambient, specular,rows, columns, scaleX, scaleY, scaleZ, rotateAngle, axis, translateX, translateY, translateZ + ((scaleX * columns) + (.1 * (columns - 1)))/2);
    renderStraightWall(ambient, specular,rows, columns, scaleX, scaleY, scaleZ, rotateAngle + 3.14159, axis, -translateX, translateY, -translateZ + ((scaleX * columns) + (.1 * (columns - 1)))/2);
    renderStraightWall(ambient, specular,rows, columns, scaleX, scaleY, scaleZ, rotateAngle + 1.5708, axis, -translateZ, translateY, translateX + ((scaleX * columns) + (.1 * (columns - 1)))/2);
    renderStraightWall(ambient, specular,rows, columns, scaleX, scaleY, scaleZ, rotateAngle + 4.71239, axis, translateZ, translateY, -translateX + ((scaleX * columns) + (.1 * (columns - 1)))/2);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glBindTexture(GL_TEXTURE_2D, texture);
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            forward = true;
            break;
        case Qt::Key_A:
            left = true;
            break;
        case Qt::Key_S:
            back = true;
            break;
        case Qt::Key_D:
            right = true;
            break;
        case Qt::Key_Tab:
            if(flyMode){
                flyMode = false;
            }
            else{
                flyMode = true;
            }
            break;
        case Qt::Key_Shift:
            down = true;
            break;
        case Qt::Key_Space:
            up = true;
            break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            forward = false;
            break;
        case Qt::Key_A:
            left = false;
            break;
        case Qt::Key_S:
            back = false;
            break;
        case Qt::Key_D:
            right = false;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            break;
        case Qt::Key_Shift:
            down = false;
            break;
        case Qt::Key_Space:
            up = false;
            break;
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;
    pitch -= d.y * sensitivity;
    yaw -= d.x * sensitivity;

    pitchMatrix = glm::rotate(mat4(1.0f),DegreeToRadian(pitch),vec3(1,0,0));
    yawMatrix = glm::rotate(mat4(1.0f), DegreeToRadian(yaw),vec3(0,1,0));

    lastPt = pt;
}

float GLWidget::DegreeToRadian(float degrees){
    return (3.14159265359 * degrees) / 180.0;
}

void GLWidget::animate(){
    // increment time by our time step
    float dt = .016;
    time += dt;

    // we want 2 seconds of animation per rotation then to start over, so
    // restart to 0 once we've reached our max time
    if(time > 2*(rotations.size()-1)) {
        time = 0;
    }

    // Convert time to a value between 0 and 1
    // at 0 we're at the beginning of our rotations
    // array, and at 1 we've reach the last one
    float t = fmin(time/(2*(rotations.size()-1)),1);

    // Get two indices into our rotations array
    // that represent our current animation
    unsigned int fromIndex = t*(rotations.size()-1);
    unsigned int toIndex = fromIndex+1;

    // when t = 1 toIndex will be out of range, so
    // just clamp it to the last index in the array
    if(toIndex > rotations.size()-1) {
        toIndex = rotations.size()-1;
    }

    // we want t to be a 0-1 value that represents the
    // percentage between two consecutive indices, so
    // get our current index as a floating point number
    // then subtract off the integer portion
    t = t*(rotations.size()-1)-(int)(t*(rotations.size()-1));

    // Euler angle representations of
    vec3 from = rotations[fromIndex];
    vec3 to = rotations[toIndex];

    // Part 1 - Variables 'to' and 'from' represent Euler rotations.
    // Construct two matrices, one using 'from' and one using 'to'.
    // Make sure the order of rotations is rotZ*rotY*rotX to match up
    // with later parts.\

    mat4 rotZ = glm::rotate(mat4(1.f), from.z, vec3(0,0,1));
    mat4 rotY = glm::rotate(mat4(1.f), from.y, vec3(0,1,0));
    mat4 rotX = glm::rotate(mat4(1.f), from.x, vec3(1,0,0));

    mat4 fromMat = rotZ * rotY * rotX;

    rotZ = glm::rotate(mat4(1.f), to.z, vec3(0,0,1));
    rotY = glm::rotate(mat4(1.f), to.y, vec3(0,1,0));
    rotX = glm::rotate(mat4(1.f), to.x, vec3(1,0,0));

    mat4 toMat = rotZ * rotY * rotX;


    matrixLerp = (1 - t) * fromMat + t * toMat;

    // Then linearly interpolate the two matrices using variable 't',
    // storing the result in matrixLerp. See how matrixLerp is used
    // in paintGL. Compile and run the program to see how

    // Part 2 - The next step is to linearly interpolate the two euler
    // angles prior to converting it to a matrix. Create a vec3 variable
    // and use it to store a linearly interpolated combination of from
    // and to.

    vec3 newRotate = (1 - t) * from + t * to ;

    rotZ = glm::rotate(mat4(1.f), newRotate.z, vec3(0,0,1));
    rotY = glm::rotate(mat4(1.f), newRotate.y, vec3(0,1,0));
    rotX = glm::rotate(mat4(1.f), newRotate.x, vec3(1,0,0));

    eulerLerp = rotZ * rotY * rotX;

    //
    // Then construct a matrix using the linearly interpolated Euler angles
    // and store it in eulerLerp. eulerLerp is used in paintGL to render
    // another of the three cubes. Notice how the second cube is just being
    // rotated rather than scaled and skewed.

    // Part 3 - Quaternions are another way to represent orientation.
    // glm has a quaternion data structure called quat. It's constructor
    // can take a vec3 that represents Euler angles. Construct two quaternions
    // using the from and to euler angles.

        glm::quat qFrom = glm::quat(from);
        glm::quat qTo = glm::quat(to);
        glm::quat q = glm::slerp(qFrom, qTo, t);

        quatSlerp = glm::toMat4(q);



    // Interpolate the two quaternions using glm::slerp. slerp stands for
    // spherical linear interpolation and is how quaternions can be animated
    // along the shortest path. glm::slerp takes 3 arguments:
    // glm::slerp(glm::quat q1, glm::quat q2, float t)
    // where t is in the range 0-1 and returns a quaternion t percent
    // between q1 and q2

    // The last step is to convert the resulting quaternion into a matrix
    // for use in our fragment shader. Use glm::toMat4(glm::quat) to do so
    // and store the resulting matrix in quatSlerp. Again, quatSlerp is used
    // in paintGL to render our third cube.

    float x = 0;
    float y = 0;
    float z = 0;
    float scale = .016;

    if(forward){
        z --;
    }
    if(back){
        z ++;
    }
    if(left){
        x --;
    }
    if(right){
        x ++;
    }
    if(up){
        y ++;
    }
    if(down){
        y --;
    }
    if(!flyMode){
        glm::vec4 r = yawMatrix * glm::vec4(1,0,0,0) * x * scale;
        glm::vec4 f = yawMatrix * glm::vec4(0,0,1,0) * z * scale;
        glm::vec4 u = yawMatrix * glm::vec4(0,1,0,0) * y * scale;

        position += vec3(r) + vec3(f) + vec3(u);

        mat4 translation = glm::translate(mat4(1.0f),position);
        mat4 cam = translation * yawMatrix * pitchMatrix;
        mat4 view = glm::inverse(cam);

        glUseProgram(cubeProg);
        glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(view));

        glUseProgram(brickProg);
        glUniformMatrix4fv(brickViewMatrixLoc, 1, false, value_ptr(view));

        glUseProgram(gridProg);
        glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(view));
    }
    else{
        glm::vec4 r = yawMatrix * glm::vec4(1,0,0,0) * x * scale;
        glm::vec4 f = yawMatrix * pitchMatrix * glm::vec4(0,0,1,0) * z * scale;
        glm::vec4 u = yawMatrix * pitchMatrix * glm::vec4(0,1,0,0) * y * scale;

        position += vec3(r) + vec3(f) + vec3(u);

        mat4 translation = glm::translate(mat4(1.0f),position);
        mat4 cam = translation * yawMatrix * pitchMatrix;
        mat4 view = glm::inverse(cam);

        glUseProgram(cubeProg);
        glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(view));

        glUseProgram(brickProg);
        glUniformMatrix4fv(brickViewMatrixLoc, 1, false, value_ptr(view));

        glUseProgram(gridProg);
        glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(view));

    }


    update();
}
